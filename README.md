# Modbot Web API Wrapper

## About

Copyright Modbot Inc.
Contact: hello@modbot.com  

This repo contains a wrapper for the Modbot web APIs in C++.


## Documentation Generation

For documentation, please compile the html documentation source with the target `make docs`. INSTALL, USAGE and CHANGELOG documents are available in the `docs` directory.

### Requirements

- sphinx
  - brew: brew install sphinx-doc
  - linux: apt-get install python3-sphinx