Modbot API
===========

Introduction
------------

The Modbot API provides a simple interface for communicating with a Modbot Brain and controlling Modbot-compatible software and hardware. It is currently available in C++.

Installation
------------
.. toctree::
   :maxdepth: 2

   INSTALL
   CHANGELOG

C++ API
------------
.. toctree::
   :caption: C++ API
   :maxdepth: 5

   cpp/index
   cpp/examples