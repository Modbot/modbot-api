=============
Installation
=============

Via the Conan Package Manager
=============================
The Modbot-API is available as a Conan package.
To use the Modbot-API Conan package, simply add the Modbot conan remote to your Conan remote list, and list the Conan package as a requirement in your conan-compatible project.

* Add the conan remote to your system

  * ``conan remote add modbot https://conan.modbot.com``

* Include the Modbot-API package as a dependency in your conan-compatible project

  * Add ``modbot-api/[version]@modbot/public`` to your ``conanfile.txt`` or ``conanfile.py``.

  * See _conan.io for more information on the Conan Package Manager.


.. _conan.io: https://conan.io/


Build from Source
==================
The Modbot-API can also be built from source.
Conan is used to download the Modbot-API wrapper's dependencies.

* Add the conan remote

  * ``conan remote add modbot https://conan.modbot.com``

* Clone the latest stable release:

  * ``git clone https://bitbucket.org/Modbot/modbot-api.git``

* Install dependencies, build library and install to `/usr/local/lib`:

  * ``make cpp`