# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [2.0.0] - 2019-08-01

### Added

- Added wrapper library for Modbot Brain V2 web APIs -- HTTP and websockets.

### Removed

- Removed 0.x API



## [0.6.5] - 2018-09-19

### Changed

- Changed order of setting JointControlMode and Enabling Operation state. Servos will not change state until the control mode is set and the brain requires that state change happens immediately. This change speeds up that sequence.
- Introduced new Servo Control Mode for starting the commutation angle offset calculation.

## [0.6.4] - 2018-08-20

### Added

- Support for Beckhoff EL1008, EL2008, and EL3004.

## [0.6.3] - 2018-08-17

### Fixed

- `getServoAnalogInputs()` response now correctly parsed.
- Fixed issue where seuqnecer and logger service stubs were not properly initialized in C++ wrapper.


## [0.6.2] - 2018-08-08

### Changed

- Conan dependency channels changed to `public`.
- Python and C++ API `runRobot()` methods include delay after servo control mode to prevent overcurrenting in hardware.


## [0.6.1] - 2018-08-01

### Fixed

Python:

- `getServoAnalogInputs(...)` now correctly initializes analog IO list.

## [0.6.0] - 2018-07-31

### Added

- Doxygen documentation generator (supports C++ docstrings).
- Sphinx documentation generator (supports python docstrings via `sphinx-apidoc`, Markdown via `recommonmark`, and doxygen xml via `breathe`).
- Sphinx documentation files
  - `index.rst`
  - `USAGE.rst`
  - `INSTALL.rst`
- `make docs` Makefile target generates `html` documentation
- `requirements.txt` for pip requirements.
- Supports `LoggerService`.
- Supports `SequencerService`.
- Added `ModbotCoreService` in anticipation of Brain V2.

C++:

- Added `LoggerService` `void logMessage(std::string message, LogLevel log_level)` method.
- Added `SequencerService` `bool getSequenceStatus()` method.
- Added `SequencerService` `void haltSequence()` method.
- Added `SequencerService` `void runSequence(std::string sequence)` method.

Python:

- Added `LoggerService` `logMessage(message, log_level)` method.
- Added `SequencerService` `getSequenceStatus()` method.
- Added `SequencerService` `haltSequence()` method.
- Added `SequencerService` `runSequence(sequence)` method.
- Added `__getActor`, `__getRobot`, `__setActor`, `__setRobot` private helper methods.

### Changed

- Documentation files moved to `docs` directory.
- C++ wrapper docstrings updated.
- Python wrapper docstrings updated.
- Python wrapper no longer has `modbot` module. Now `Modbot` and other classes are directly within `modbot` library. `modbot.modbot.Modbot()` is now `modbot.Modbot()`.
- Changed `JointState` to `CoordinatedTargets` as a more descriptive name.
- Changed `Module` object to `Actor` to reflect coming Brain changes.
- Python API log formatting updated.
- Deprecated `modbot-api.proto` `ModbotCore` service to `modbot-core-service-deprecated` in anticipation of Brain V2.

### Fixed

- Fixed unmerged `python-library-uninstall` fix: target no longer fails when no library was previously installed.


## [0.5.0] - 2018-07-05

### Added

- Makefile target `python-runtime-dependencies` to install pip packages `protobuf`, `grpc`, and `grpcio`.
- `version.txt`

### Changed

- Clarified building / installation instructions in README.
- Changed directory structure to be consistent with other Modbot libraries. Include path is now `modbot/modbot-api/modbot-api.h`.
- Changed Makefile targets to be consistent with other Modbot libraries.

### Fixed

- Re-added `conanfile.txt` and imports for `make install-dependencies`.
- Fixed dependencies of `grpc` and `protobuf` listed as `build-requires` rather as general `requires`.
- Fixed c++ library installation on OS X.
- `conanfile.py`, `setup.py`, and `CMakeLists.txt` now appropriately load from `version.txt`.


## [0.4.1] - 2018-06-04

### Fixed

Python:

- Fixed printing error in runRobot(...)
- Call `python -m pip` rather than `pip` to install library.

## [0.4.0] - 2018-05-30

### Added

C++:

- `ControlModeCommand` struct has been added to represent a ControlMode for a specific servo.
- `runRobot(...)` has been overloaded to accept a vector of ControlModeCommands.

Python:

- `runRobot(...)` can now accept a list of `[servo_id, control_mode]` lists.

### Changed

- `make <language>` now runs `clean` and `install-dependencies` as well.


## [0.3.0] - 2018-05-11

### Changed

Proto:

- `Node` has been renamed, in messages and service methods, to `Module`.
- `Joint` has been renamed, in messages and service methods, to `Servo`.
- `JointControlMode` has been renamed, in messages and service methods, to `ControlMode`.
 - `JointParameterRequest` has been changed to `HardwareParameterRequest`. The `value` parameter has been changed from `sint32` to `double`.
 - `updateJointParameter` has been changed to `updateHardwareParameter`.

C++:

- `Node` has been renamed to `Module`.
- `Joint` has been renamed to `Servo`.
- `JointControlMode` has been renamed to `ControlMode`.
- API supports multiple robots. Robot-specific API calls now require `robot_id`.
- `robotRun(...)` has been changed to `runRobot(...)` to be consistent with other function names.
- `runRobot(...)` now accepts an optional control mode. If no control mode is specified, a default of `CSP` is used.
- `ControlMode` values have been changed from `POSITION`, `VELOCITY`, `TORQUE` to `UNKNOWN`, and `CSP`, `CSV`, `CST`, respectively.

Python:

- `Node` has been renamed to `Module`.
- `Joint` has been renamed to `Servo`.
- `JointControlMode` has been renamed to `ControlMode`.
- API supports multiple robots. Robot-specific API calls now require `robot_id`.
- `robotRun(...)` has been changed to `runRobot(...)` to be consistent with other function names.
- `runRobot(...)` now accepts an optional control mode. If no control mode is specified, a default of `CSP` is used.
- `ControlMode` values have been changed from `POSITION`, `VELOCITY`, `TORQUE` to `UNKNOWN`, and `CSP`, `CSV`, `CST`, respectively.

### Added

C++:

- API supports any server address. Address is specified during Modbot class instantiation.
- Added support for updating Servo Hardware Parameters via `setServoHardwareParameter(...)`.
- `std::vector<std::unique_ptr<Robot>> listRobots()`
- `std::vector<std::unique_ptr<Modules>> listModules(unsigned int robot_id)`

Python:

- Log statements.
- API supports any server address. Address is specified during Modbot class instantiation.
- Added support for updating Servo Hardware Parameters via `setServoHardwareParameter(...)`.
- `listRobots()` - returns a list of modbot robots.
- `listModules(robot_id)` - returns a list of modbot modules.

### Fixed
C++:

- `runRobot(...)` waits before setting control mode, allowing the slaves to enter OP.
- "GetX" methods correctly return information from response.


## [0.2.1] - 2018-04-05
### Added
General:

- Makefile target `make clean`
- Compatibility building with MacOSX using `DYLD_LIBRARY_PATH` in Makefile recipes.

### Fixed
C++:

- C++ library is now appropriately namespaced to `modbot`.
- Fixed issue where protobuf value wasn't statically cast.
- Fixed const `ROBOT_ID`
- Fixed `getJointCount()` return count.
- Fixed `getJointTargetPosition()` input params.

Conan:

- Packaged `*.h` directory structure is now correct.

## [0.2.0] - 2018-03-06

### Added
General:

- C++ library consistently named `modbot-api`
- Python library renamed to `modbot`
- Added versioning to CMake.
- Added `CHANGELOG.md`
- `make clean` no longer clears `bin`.
- `make cpp` and `make python` no longer `install-dependencies`. 

Proto Message Defintions:

- Robot
  - `robot_id` field has been changed to `id`
  - the `RobotControlMode` enum has an added `UNKNOWN` field of value 0.

- Node
  - `module_id` field has been changed to `id`
  - added repeated `analog_inputs`
  - added repeated `analog_outputs`
  - added repeated `digital_inputs`
  - added repeated `digital_outputs`

- Joint
  - `digital_input` and `digital_output` has been moved to the parent `Node`
  - the `JointControlMode` enum has an added `JOINTCONTROLMODE_UNKNOWN` field of value 0.
  - the `JointFault` enum has an added `JOINTFAULT_UNKNOWN` field of value 0.
  - removed unimplemented `SDO`.

- DigitalInput / DigitalOutput
  - messages have been consolidated into `DigitalIO`
  - `state` field has changed to `value`.

- AnalogIO: message type created.
- DigitalIO: message type created.

C++ Wrapper:

- added `getJointAnalogInputs(...)`
- `getJointDigitalInput(...)` has changed to `getJointDigitalInputs(...)` and returns all digital inputs.
- `getJointDigitaOutputs(...)` has changed to `getJointDigitalOutputs(...)` and returns all digital outputs.

## [0.1.1]
API Prototype submitted.
