.PHONY: Makefile apidoc clean doxygen install-dependencies protobuf source

OS:=$(shell uname -s)
LLP="LD_LIBRARY_PATH"

ifeq ($(OS),Darwin)
	LLP="DYLD_LIBRARY_PATH"
endif

# Configcd cd
PWD												= $(shell pwd)
PY_API_DIR    						= ../python/modbot
BUILD_DIR     						= build
DOC_SOURCE_DIR    				= source

# Doxygen
DOXYGEN_OPTS    					=
DOXYGEN_BUILD   					= doxygen
DOXYGEN_CONFIG						= Doxyfile
DOXYGEN_BUILD_DIR  				= $(BUILD_DIR)/doxygen

PROTOC										= ../bin/protoc
PROTO_INCLUDE							= ../proto
PROTODOC_OUTPUT_PATH						= $(DOC_SOURCE_DIR)/proto

# Sphinx
SPHINX_OPTS    						=
SPHINX_BUILD   						= sphinx-build
SPHINX_PROJ    						= ModbotAPI
SPHINX_OUTPUT_PATH				= $(BUILD_DIR)/sphinx
SPHINX_APIDOC   					= sphinx-apidoc
SPHINX_APIDOC_OPTS				= -f -E -M -e
SPHINX_APIDOC_OUTPUT_PATH = $(DOC_SOURCE_DIR)/python/apidoc
# Exclude needs to be abs path -- possible apidoc bug.
SPHINX_APIDOC_EXCLUDE			= $(shell pwd)/$(PY_API_DIR)/*_pb2*.py $(shell pwd)/$(PY_API_DIR)/version.py

apidoc:
	mkdir -p $(SPHINX_OUTPUT_PATH)
	$(SPHINX_APIDOC) $(SPHINX_APIDOC_OPTS) -o $(SPHINX_APIDOC_OUTPUT_PATH) $(PY_API_DIR) $(SPHINX_APIDOC_EXCLUDE)

autogen:
	sphinx-autogen -o $(DOC_SOURCE_DIR)/python/_autosummary $(DOC_SOURCE_DIR)/python/index.rst

clean:
	rm -rf $(BUILD_DIR) $(PROTODOC_OUTPUT_PATH) $(SPHINX_APIDOC_OUTPUT_PATH) $(DOC_SOURCE_DIR)/python/_autosummary

doxygen:
	mkdir -p $(DOXYGEN_BUILD_DIR)
	$(DOXYGEN_BUILD) "$(PWD)/$(DOXYGEN_CONFIG)"

install-dependencies:
	python -m pip install --user -r requirements.txt
source: doxygen

%: clean source
	$(SPHINX_BUILD) -b $@ "$(DOC_SOURCE_DIR)" "$(BUILD_DIR)/$@" $(SPHINX_OPTS) $(O)