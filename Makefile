# Copyright 2017 Modbot Inc.
.PHONY: clean install-dependencies docs lint \
	cpp cpp-library-generate cpp-library-build cpp-library-install cpp-library-uninstall \
	python python-library-generate python-library-install python-library-uninstall python-runtime-dependencies python-library-update-version

BUILD = cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ../src -G "Unix Makefiles" && make

NPROCS:=1
OS:=$(shell uname -s)
LLP = "LD_LIBRARY_PATH"
LIB_EXT = so

MAKEFILE_PY = python/Makefile

ifeq ($(OS),Linux)
	NPROCS := $(shell grep -c ^processor /proc/cpuinfo)
else ifeq ($(OS),Darwin)
	NPROCS := $(shell sysctl hw.ncpu | awk '{print $$2}')
	LLP = "DYLD_LIBRARY_PATH"
	LIB_EXT = dylib
endif

all: conan

clean:
	rm -rf bin build doc
	$(MAKE) -f $(MAKEFILE_PY) clean
	cd docs && $(MAKE) clean

conan:
	conan create . modbot/latest

configure:
	mkdir -p build
	cd build && conan install --build=outdated .

lint:
	cpplint --recursive --exclude=src/modbot/protobuf/* src/

# Documentation
docs:
	cd docs && $(MAKE) install-dependencies
	cd docs && $(MAKE) html

# C++
cpp: clean configure cpp-library-build cpp-library-uninstall cpp-library-install

cpp-library-build:
	mkdir -p build
	cd build && $(BUILD) -j$(NPROCS)

cpp-library-install:
	cd build && sudo make install

cpp-library-uninstall:
	sudo rm -rf /usr/local/lib/libmodbot-api*
	sudo rm -rf /usr/local/include/modbot/modbot-api*