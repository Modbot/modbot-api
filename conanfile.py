from conans import ConanFile, CMake, tools
from os.path import exists

class ModbotApi(ConanFile):
    name = 'modbot-api'
    version_file = 'version.txt'
    version = tools.load(version_file).strip() if exists(version_file) else ''

    build_policy = 'missing'
    default_options = (
      'shared=True',
      'fPIC=True',
      'asio:with_openssl=True',
      'modbot-logging:shared=True',
      'modpp:shared=True',
      'yaml-cpp:shared=True'
    )
    exports_sources ='include*', 'src*', 'CMakeLists.txt', 'version.txt'
    generators = 'cmake'
    options = {'shared': [True, False], 'fPIC': [True, False]}
    requires = (
      'jsonformoderncpp/3.2.0@vthiery/stable',
      'modbot-logging/2.1.3@modbot/stable',
      'modpp/0.0.11@modbot/stable',
      'simple-web-server/master@modbot/latest',
      'simple-websocket-server/2.0.0-rc4@modbot/stable',
      'yaml-cpp/master@modbot/latest'
    )
    settings = 'os', 'compiler', 'build_type', 'arch'
    url = 'https://bitbucket.org/Modbot/modbot-api'


    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package_info(self):
        self.cpp_info.libs = ['modbot-api']

    def package(self):
        self.copy("*.h*", dst="include/modbot-api", keep_path=False)
        self.copy(pattern="*.so*", dst="lib", keep_path=False)
        self.copy(pattern="*.a", dst="lib", keep_path=False)

# [generators]
# cmake

# [requires]
# docopt.cpp/0.6.2@modbot/stable
# jsonformoderncpp/3.2.0@vthiery/stable
# modbot-logging/2.1.1@modbot/stable
# modpp/0.0.9@modbot/stable
# simple-web-server/master@modbot/latest
# simple-websocket-server/2.0.0-rc4@modbot/stable
# yaml-cpp/master@modbot/latest

# [options]
# asio:with_openssl=True
# modbot-logging:shared=True
# modpp:shared=True
# yaml-cpp:shared=True

# [imports]
# lib, *.dylib -> lib
# lib, *.so* -> lib