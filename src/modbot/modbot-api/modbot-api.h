/**
 * Copyright 2019 Modbot Inc.
 */

#ifndef SRC_MODBOT_API_H_
#define SRC_MODBOT_API_H_

#include <algorithm>

#include <modbot/logging/logging.h>
#include <modpp/observable.h>
#include <modpp/thread.h>
#include <nlohmann/json.hpp>
#include <simple-web-server/client_http.hpp>
#include <simple-websocket-server/client_ws.hpp>
#include <yaml-cpp/yaml.h>


namespace modapi {

using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using SubscriptionCallback = std::function<void(nlohmann::json&)>;
using WsClient = SimpleWeb::SocketClient<SimpleWeb::WS>;
using WsConnection = WsClient::Connection;

const unsigned int kJsonSpaces = 2;
const std::string kModuleCore = "CoreApiModule";
const std::string kModuleSequencer = "SequencerModule";
const unsigned int kModbotDefaultSubscriptionPeriod = 500;  // ms

///// ----- Structures

struct sequenceIndex {
  int sequence_pointer;
  int sequence_size;
};

class ModbotClient {
 public: // ----- Public Functions
  ModbotClient();
  ~ModbotClient();

  bool connect(std::string api_endpoint, std::string port = "80");
  bool disconnect();
  bool mapModules();
  void onConnect(std::function<void()> on_connect);
  void onDisconnect(std::function<void()> on_disconnect);

  // WS
  bool parseMessage(nlohmann::json& json_message);
  bool subscribe(std::string topic, SubscriptionCallback callback, unsigned int period = kModbotDefaultSubscriptionPeriod);
  bool unsubscribe(std::string topic, unsigned int period = kModbotDefaultSubscriptionPeriod);
  bool wsRequest(nlohmann::json& body);
  void websocketOnClose(std::shared_ptr<WsClient::Connection> connection, int status = 1000, const std::string& reason = "");
  void websocketOnError(std::shared_ptr<WsClient::Connection> connection, const SimpleWeb::error_code &ec);
  void websocketOnMessage(std::shared_ptr<WsClient::Connection> connection, std::shared_ptr<WsClient::InMessage> message);
  void websocketOnOpen(std::shared_ptr<WsClient::Connection> connection);


  // CRUD requests
  bool httpRequest(std::string requestType, std::string endpoint, nlohmann::json* response = nullptr, std::string body = "");
  bool get(std::string endpoint, std::string property, std::string* response);
  bool get(std::string endpoint, std::vector<std::string> properties, std::map<std::string, std::string>* response);
  bool list(std::string endpoint, nlohmann::json* response);
  bool listQuery(std::string endpoint, std::string property, std::string value, nlohmann::json* response);
  bool listIds(std::string endpoint, std::vector<unsigned int>* response);
  bool listIdsQuery(std::string endpoint, std::string property, std::string value, std::vector<unsigned int>* response);
  bool patch(std::string endpoint, nlohmann::json& body);
  bool patch(std::string endpoint, std::string property, std::string value);


  // API convenience methods
  std::string endpoint(std::string type, unsigned int id);
  std::string moduleEndpoint(std::string name);
  unsigned int moduleId(std::string name);

  //  Core
  bool getConnectionStatus();
  bool getCoreLiveStatus(int* response);
  bool getCorePartsConnectedStatus(int* response);

  //  Sequencer
  bool getSequencerIndex(sequenceIndex* response);
  bool getSequencerStatus(int* status);
  bool playSequence(std::string sequence_string);
  bool stopSequence();

 private:
  std::unique_ptr<HttpClient> client_http_;
  std::unique_ptr<WsClient> client_ws_;
  std::unique_ptr<modpp::Thread> ws_thread_;
  std::weak_ptr<WsConnection> client_ws_connection_;
  SimpleWeb::CaseInsensitiveMultimap http_header_;
  std::map<std::string, unsigned int> module_ids_;
  std::map<std::string, std::vector<SubscriptionCallback>> ws_subscriptions_;

  modpp::Observable<bool> connection_status_http_{false};
  modpp::Observable<bool> connection_status_ws_{false};
  std::function<void()> on_connect_;
  std::function<void()> on_disconnect_;

  void internalOnConnect();
  void internalOnDisconnect();
}; // class

}; // namespace modapi

#endif  // SRC_MODBOT_API_H_
