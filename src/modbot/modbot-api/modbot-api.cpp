/**
 * Copyright 2019 Modbot Inc.
 */

#include "modbot-api.h"

using namespace modapi;
using namespace modpp;
using namespace modpp::literals;
using namespace std::placeholders;
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using json = nlohmann::json;

MB_DEFINE_LOG_CATEGORY(log_api, "api");
MB_DEFINE_LOG_CATEGORY(log_api_http, "api-http");
MB_DEFINE_LOG_CATEGORY(log_api_ws, "api-ws");


ModbotClient::ModbotClient() {
  http_header_ = {
    {"Accept", "application/json"},
    {"Content-Type", "application/json; charset=utf-8"}
  };
}

ModbotClient::~ModbotClient() {
  // Disconnect
  client_ws_->stop();
  if (ws_thread_ != nullptr && ws_thread_->joinable()) {
    ws_thread_->join();
  }

  auto on_connection_status_changed = [this] (bool status) {
    if (!status) internalOnDisconnect();

    if (connection_status_http_ && connection_status_ws_) internalOnConnect();
  };
  connection_status_http_.changed.connect(on_connection_status_changed);
  connection_status_ws_.changed.connect(on_connection_status_changed);
}

void ModbotClient::internalOnConnect() {
  MB_TRACE(log_api) << "Connected";
  usleep(2000000);
  mapModules();
  if (on_connect_) on_connect_();
}

void ModbotClient::internalOnDisconnect() {
  if (on_disconnect_) on_disconnect_();
}

bool ModbotClient::connect(std::string api_endpoint, std::string port) {
  std::string http_endpoint = api_endpoint + ":" + port;
  std::string ws_endpoint =  api_endpoint + ":" + port + "/ws";
  MB_TRACE(log_api) << "Connecting...";
  MB_TRACE(log_api) << " - HTTP: " << http_endpoint;
  MB_TRACE(log_api) << " - WS: " << ws_endpoint;
  bool status = true;
  client_http_ = std::make_unique<HttpClient>(http_endpoint);
  client_ws_ = std::make_unique<WsClient>(ws_endpoint);


  client_ws_->on_close = std::bind(&ModbotClient::websocketOnClose, this, _1, _2, _3);
  client_ws_->on_error = std::bind(&ModbotClient::websocketOnError, this, _1, _2);
  client_ws_->on_message = std::bind(&ModbotClient::websocketOnMessage, this, _1, _2);
  client_ws_->on_open = std::bind(&ModbotClient::websocketOnOpen, this, _1);

  MB_TRACE(log_api) << " - Clients created";

  ws_thread_ = std::make_unique<modpp::Thread>();
  ws_thread_->start([this] {
    MB_INFO(log_api_ws) << "Websockets thread starting";
    client_ws_->start();
    MB_INFO(log_api_ws) << "Websockets thread finished";
  });

  // client_ws_->start();
  MB_TRACE(log_api) << " - Websockets client started";
  status &= mapModules();
  return status;
}

bool ModbotClient::disconnect() {
  bool status = true;

  MB_TRACE(log_api) << "Disconnecting...";

  client_ws_->stop();
  MB_TRACE(log_api) << " - Stopped websockets client";

  ws_thread_->join();
  MB_TRACE(log_api) << " - Joined websockets thread";

  client_http_.reset();
  MB_TRACE(log_api) << " - Destroyed http client";

  client_ws_.reset();
  MB_TRACE(log_api) << " - Destroyed websockets client";
  MB_TRACE(log_api) << " - Disconnected";

  connection_status_http_ = false;
  connection_status_ws_ = false;

  return status;
}

// WS

bool ModbotClient::wsRequest(json& body) {
  MB_TRACE(log_api_ws) << "Sending request: " << body;
  std::string request = body.dump();

  if (auto ws = client_ws_connection_.lock()) {
    ws->send(request);
    return true;
  }

  MB_WARNING(log_api_ws) << "Failed to send Websockets request - no connection";
  return false;
}

bool ModbotClient::parseMessage(json& json_message) {
   MB_DEBUG(log_api_ws) << "Parsing message " << json_message.dump(kJsonSpaces);
  // Batch
  if (json_message.is_array()) {
    MB_TRACE(log_api_ws) << " - Received batched message";
    bool status = true;
    for (auto& batched : json_message) {
      MB_DEBUG(log_api_ws) << " - Parsing batched message " << batched.dump(kJsonSpaces);
      status &= parseMessage(batched);
    }
    return status;
  }

  if (json_message.find("topic") == json_message.end()) {
    MB_WARNING(log_api_ws) << "Failed to parse message - no topic";
    return false;
  }

  std::string topic = json_message["topic"].get<std::string>();
  if (ws_subscriptions_.find(topic) == ws_subscriptions_.end()) {
    MB_WARNING(log_api_ws) << "No subscription for topic [" << topic << "]";
    return true;
  }

  size_t i = 0;
  json json_data = json_message["data"];
  json json_success = json_message["success"];
  if (!json_success.is_null()) {
    if (!json_success.get<bool>()) {
      std::stringstream err;
      err << "Failed to manage subscription for topic [" << topic << "]";
      if (!json_message["type"].is_null()) {
        err << " (type " << json_message["type"].get<unsigned int>() << ")";
      }
      if (!json_message["message"].is_null()) {
        err << " - " << json_message["message"].get<std::string>();
      }
      MB_WARNING(log_api_ws) << err.str();
    }
    return json_success.get<bool>();
  }

  for (auto& callback : ws_subscriptions_[topic]) {
    MB_TRACE(log_api_ws) << "Executing topic [" << topic << "] callback [" << i << "]";
    callback(json_data);
  }
  return true;
}

bool ModbotClient::subscribe(std::string topic, SubscriptionCallback callback, unsigned int period) {
  MB_INFO(log_api) << "Subscribing to topic [" << topic << "] (" << period << " ms)";
  json json_request;
  json_request["period"] = period;
  json_request["topic"] = topic;
  json_request["type"] = 1;

  if (ws_subscriptions_.find(topic) == ws_subscriptions_.end()) {
    ws_subscriptions_[topic] = std::vector<SubscriptionCallback>();
  }
  ws_subscriptions_[topic].push_back(callback);

  return wsRequest(json_request);
}

bool ModbotClient::unsubscribe(std::string topic, unsigned int period) {
  MB_INFO(log_api) << "Unsubscribing from topic [" << topic << "] (" << period << " ms)";
  json json_request;
  json_request["period"] = period;
  json_request["topic"] = topic;
  json_request["type"] = 0;

  if (ws_subscriptions_.find(topic) != ws_subscriptions_.end()) {
    ws_subscriptions_.erase(topic);
  }

  return wsRequest(json_request);
}

void ModbotClient::websocketOnClose(
    std::shared_ptr<WsConnection> connection,
    int status,
    const std::string& reason) {
  MB_INFO(log_api_ws) << "Websockets connection closed";
  connection_status_ws_ = false;
}

void ModbotClient::websocketOnError(std::shared_ptr<WsClient::Connection> connection, const SimpleWeb::error_code &ec) {
  MB_ERROR(log_api_ws) << "Websockets connection error: " << ec << ", message: " << ec.message();
  connection_status_ws_ = false;
}

void ModbotClient::websocketOnMessage(
    std::shared_ptr<WsConnection> connection,
    std::shared_ptr<WsClient::InMessage> message) {
  json json_message = json::parse(message->string());
  MB_DEBUG(log_api_ws) << "On message " << json_message.dump(kJsonSpaces);
  parseMessage(json_message);
}

void ModbotClient::websocketOnOpen(std::shared_ptr<WsClient::Connection> connection) {
  MB_INFO(log_api_ws) << "Websockets connection opened";
  client_ws_connection_ = connection;
  connection_status_ws_ = true;
}

// HTTP

bool ModbotClient::httpRequest(std::string requestType, std::string endpoint, json* response, std::string body) {
  try {
    MB_DEBUG(log_api_http) << "HTTP request - [" << requestType << "] " << endpoint << " " << body;
    auto response_raw = client_http_->request(requestType, endpoint, body, http_header_);
    *response = json::parse(response_raw->content.string());
    MB_DEBUG(log_api_http) << "HTTP response - [" << requestType << "] " << endpoint << " " << body << ": " << *response;

    connection_status_http_ = true;
    return true;

  } catch (const std::system_error& err) {
    MB_WARNING(log_api_http) << "Failed to make HTTP request - [" << err.code() << "] \"" << err.what() << "\"";
    MB_INFO(log_api_http) << "Failed to make HTTP request - [" << requestType << "] " << endpoint << " " << body;

    if (static_cast<int>(err.code().value()) == 2) {
      connection_status_http_ = true;

    } else {
      connection_status_http_ = false;
    }

    return false;

  } catch (const std::exception& err) {
    MB_ERROR(log_api_http) << "Failed to make HTTP request (unknown error) - \"" << err.what() << "\"";
    connection_status_http_ = false;
    return false;
  }
}

bool ModbotClient::get(std::string endpoint, std::string property, std::string* response) {
  MB_TRACE(log_api) << "Get " << endpoint << " - property \"" << property << "\"";
  json json_response;
  bool status = httpRequest("GET", endpoint, &json_response);
  *response = json_response[property].dump();
  MB_DEBUG(log_api_http) << " - result: \"" << json_response;
  MB_TRACE(log_api) << " - result: \"" << json_response[property].dump() << "\" -> " << *response;
  return status;
}

bool ModbotClient::get(std::string endpoint, std::vector<std::string> properties, std::map<std::string, std::string>* response) {
  MB_TRACE(log_api) << "Get " << endpoint;
  std::map<std::string, std::string> values;
  json json_response;
  bool status = httpRequest("GET", endpoint, &json_response);
  for(auto property : properties) {
    MB_TRACE(log_api_http) << " - property \"" << property << "\"";
    values[property] = json_response[property].dump();
    MB_TRACE(log_api) << "   - result: \"" << values[property] << "\"";
  }
  *response = values;
  return status;
}

bool ModbotClient::list(std::string endpoint, json* response) {
  MB_TRACE(log_api) << "List " << endpoint;
  return httpRequest("GET", endpoint, response);
}

bool ModbotClient::listQuery(std::string endpoint, std::string property, std::string value, json* response) {
  MB_TRACE(log_api) << "List (query) " << endpoint << " - " << property << " = " << value;
  std::vector<unsigned int> ids;
  json json_response;
  json json_response_filtered;
  bool status = httpRequest("GET", endpoint, &json_response);

  for (json::iterator it = json_response.begin(); it != json_response.end(); ++it) {
    std::string criteria = (*it)[property].get<std::string>();
    if (criteria != value) {
      json_response_filtered.push_back(*it);
    }
  }
  *response = json_response_filtered;
  return status;
}

bool ModbotClient::listIds(std::string endpoint, std::vector<unsigned int>* response) {
  MB_TRACE(log_api) << "List ids " << endpoint;
  std::vector<unsigned int> ids;
  json json_response;
  bool status = httpRequest("GET", endpoint, &json_response);

  for (json::iterator it = json_response.begin(); it != json_response.end(); ++it) {
    unsigned int id = (*it)["id"].get<unsigned int>();
    ids.push_back(id);
    MB_TRACE(log_api) << " - [" << id << "]";
  }
  *response = ids;
  return status;
}

bool ModbotClient::listIdsQuery(std::string endpoint, std::string property, std::string value, std::vector<unsigned int>* response) {
  MB_TRACE(log_api) << "List ids (query) " << endpoint << " - " << property << " = " << value;
  std::vector<unsigned int> ids;
  json json_response;
  bool status = httpRequest("GET", endpoint, &json_response);

  for (json::iterator it = json_response.begin(); it != json_response.end(); ++it) {
    unsigned int id = (*it)["id"].get<unsigned int>();
    std::string criteria = (*it)[property].get<std::string>();
    if (criteria == value) {
      ids.push_back(id);
      MB_TRACE(log_api) << " - [" << id << "]";
    }
  }
  *response = ids;
  return status;
}

bool ModbotClient::patch(std::string endpoint, nlohmann::json& body) {
  MB_TRACE(log_api) << "Patch " << endpoint << " - " << body.dump();
  json json_response;
  return httpRequest("PATCH", endpoint, &json_response, body.dump());
}

bool ModbotClient::patch(std::string endpoint, std::string property, std::string value) {
  std::string message = "{\"" + property + "\" : \"" + value + "\"}";
  MB_TRACE(log_api) << "Patch " << endpoint << " - " << message;
  json json_response;
  return httpRequest("PATCH", endpoint, &json_response, message);
}


// API helper functions

std::string ModbotClient::endpoint(std::string type, unsigned int id) {
  return ("/" + type + "/" + std::to_string(id));
}

bool ModbotClient::getConnectionStatus() {
  return (connection_status_http_ && connection_status_ws_);
}

bool ModbotClient::getCoreLiveStatus(int* response) {
  json json_response;
  bool status = httpRequest("GET", moduleEndpoint(kModuleCore), &json_response);
  if (!status) return status;

  *response = std::stoi(json_response["live"].dump());
  return status;
}

bool ModbotClient::getCorePartsConnectedStatus(int* response) {
  json json_response;
  bool status = httpRequest("GET", moduleEndpoint(kModuleCore), &json_response);
  if (!status) return status;

  *response = std::stoi(json_response["connect_all_parts"].dump());
  return status;
}

bool ModbotClient::getSequencerIndex(sequenceIndex* response) {
  json json_response;
  bool status = httpRequest("GET", moduleEndpoint(kModuleSequencer), &json_response);
  if (!status) return status;

  response->sequence_pointer = json_response["sequence_index"].get<int>();
  response->sequence_size = json_response["sequence_size"].get<int>();
  MB_TRACE(log_api) << "Sequence index: " << json_response["sequence_index"].dump();
  MB_TRACE(log_api) << "Sequence size:  " << json_response["sequence_size"].dump();
  return status;
}

bool ModbotClient::getSequencerStatus(int* response) {
  json json_response;
  bool status = httpRequest("GET", moduleEndpoint(kModuleSequencer), &json_response);
  *response = std::stoi(json_response["status"].dump());
  return status;
}

bool ModbotClient::mapModules() {
  json json_response;
  bool status = httpRequest("GET", "/modules", &json_response);
  MB_TRACE(log_api) << "Registering modules...";
  for(size_t i = 0; i < json_response.size(); i++) {
    std::string name = json_response[i]["name"];
    unsigned int id = json_response[i]["id"];
    module_ids_[name] = id;
    MB_TRACE(log_api) << " - [" << id << "]" << name;
  }

  // Confirm critical modules exist
  const std::vector<std::string> modules = {
    kModuleCore,
    kModuleSequencer
  };
  for (auto module_name : modules) {
    if (module_ids_.find(module_name) == module_ids_.end()) {
      MB_WARNING(log_api) << "Critical module \"" << module_name << "\" not found";
    }
  }
  return status;
}

std::string ModbotClient::moduleEndpoint(std::string module_name) {
  return endpoint("modules", moduleId(module_name));
}

unsigned int ModbotClient::moduleId(std::string module_name) {
  if (module_ids_.find(module_name) == module_ids_.end()) return 0;
  return module_ids_[module_name];
}


void ModbotClient::onConnect(std::function<void()> on_connect) {
  on_connect_ = on_connect;
}

void ModbotClient::onDisconnect(std::function<void()> on_disconnect) {
  on_disconnect_ = on_disconnect;
}


bool ModbotClient::playSequence(std::string sequence_string) {
  MB_INFO(log_api) << "Sending sequence: \n" << sequence_string;
  bool status = true;
  std::replace(sequence_string.begin(), sequence_string.end(), '\n', ';');
  status &= patch(moduleEndpoint(kModuleSequencer), "sequence", sequence_string);
  usleep(100000);
  status &= patch(moduleEndpoint(kModuleSequencer), "status", "2");
  return status;
}

bool ModbotClient::stopSequence() {
  return patch(moduleEndpoint(kModuleSequencer), "status", "1");
}